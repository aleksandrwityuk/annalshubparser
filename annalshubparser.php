<?php
/*
Plugin Name: Annalshubparser
Plugin URI: https://annalshubparser.com/
Description: Business and Sprts site parser and creating wordpress posts from that
Version: 1.0.0
Author: Alex W
Author URI: http://annalshubparser.com
License:
Text Domain: annalshubparser
*/

if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define( 'ANNALSHUBPARSER__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

register_activation_hook( __FILE__, array( 'AnnalshubInit', 'annalshubparser_plugin_activation' ) );

require_once( ANNALSHUBPARSER__PLUGIN_DIR . 'class.annalshubinit.php' );
require_once( ANNALSHUBPARSER__PLUGIN_DIR . 'class.annalshubparser.php' );
