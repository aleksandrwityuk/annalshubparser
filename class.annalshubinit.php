<?php
/**
 * 
 */
class AnnalshubInit
{

    public static function annalshubparser_after_loaded() {

        $cat_args = array(
            'parent'        => 0,
            'number'        => 2,
            'hide_empty'    => false           
        );
        $terms = get_terms( 'category', $cat_args );

        foreach ($terms as $term) {
        
            $url = 'https://annalshub.com/category/' . $term->slug . '/';
            $cat_id = $term->term_id;

            new Annalshubparser( $url, $cat_id );

        }

    }

    public static function annalshubparser_plugin_activation() {

        wp_insert_term( 'Buisness', 'category',    array('slug' => 'buisness') );
        wp_insert_term( 'Sports', 'category',    array('slug' => 'sports') );

        self::annalshubparser_after_loaded();

    }
}